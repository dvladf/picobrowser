#include <QApplication>
#include <QtWebEngine>
#include "MainWindow.h"

QString getAppDataPath()
{
#ifdef WIN32
	wchar_t* path;
	_wdupenv_s(&path, nullptr, L"AppData");
	return QString::fromWCharArray(path) + "\\PicoBrowser\\settings.ini";
#endif
}

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	app.setWindowIcon(QIcon(":/icons/web-browser.svg"));
	
	QSettings settings(getAppDataPath(), QSettings::IniFormat);
	QTranslator translator;

	if (settings.value("General/Language").toString() == "Russian")
		translator.load(":/translations/ru_RU.qm");
	
	app.installTranslator(&translator);

	MainWindow w(settings);
	w.show();
	return app.exec();
}

#ifdef WIN32
#include <Windows.h>
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	return main(__argc, __argv);
}
#endif
