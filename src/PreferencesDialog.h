#pragma once

#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QNetworkProxy>
#include <QPushButton>
#include <QSettings>
#include <QSpinBox>
#include <QTabWidget>
#include <QRadioButton>

enum Language { EnglishLanguage, RussianLanguage };

class ProxyPreferencesTab;
class GeneralPreferencesTab;

class PreferencesDialog : public QDialog
{
	Q_OBJECT

	QTabWidget* tabs;
	QPushButton* ok_button;
	QPushButton* cancel_button;

	GeneralPreferencesTab* general_preferences_tab;
	ProxyPreferencesTab* proxy_preferences_tab;

public:
	PreferencesDialog(QSettings& settings, QNetworkProxy& proxy);
	void setUi();
};

class GeneralPreferencesTab : public QWidget
{
	Q_OBJECT

	QLineEdit* homepage_edit;
	QComboBox* lang_edit;

public:
	GeneralPreferencesTab(QSettings& settings);
	void getSettings(QSettings& settings);
};

class ProxyPreferencesTab : public QWidget
{
	Q_OBJECT

	QRadioButton* without_proxy_button;
	QRadioButton* use_proxy_button;
	QLineEdit* host_edit;
	QSpinBox* port_edit;
	QLineEdit* user_edit;
	QLineEdit* password_edit;

public:
	ProxyPreferencesTab(QNetworkProxy& proxy);
	void getProxy(QNetworkProxy& proxy);
};