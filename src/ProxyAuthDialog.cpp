#include "ProxyAuthDialog.h"

ProxyAuthDialog::ProxyAuthDialog(QAuthenticator* auth) : QDialog()
{
	setModal(true);
	setWindowTitle(tr("Proxy Authentication"));

	user_edit = new QLineEdit;
	password_edit = new QLineEdit;
	ok_button = new QPushButton("OK");
	cancel_button = new QPushButton(tr("Cancel"));
	password_edit->setEchoMode(QLineEdit::Password);

	ok_button->setFixedWidth(100);
	cancel_button->setFixedWidth(100);

	auto main_layout = new QVBoxLayout;
	auto user_layout = new QHBoxLayout;
	auto password_layout = new QHBoxLayout;
	auto button_layout = new QHBoxLayout;

	setLayout(main_layout);

	main_layout->addLayout(user_layout);
	main_layout->addLayout(password_layout);
	main_layout->addLayout(button_layout);
	user_layout->addWidget(new QLabel(tr("Username\t")));
	user_layout->addWidget(user_edit);
	password_layout->addWidget(new QLabel(tr("Password\t")));
	password_layout->addWidget(password_edit);
	button_layout->addWidget(ok_button);
	button_layout->addWidget(cancel_button);

	connect(ok_button, &QPushButton::clicked, [this, auth]() {
		auth->setUser(user_edit->text());
		auth->setPassword(password_edit->text());
		close();
	});
	connect(cancel_button, &QPushButton::clicked, [this]() { close(); });
}