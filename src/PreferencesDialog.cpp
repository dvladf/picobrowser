#include "PreferencesDialog.h"

PreferencesDialog::PreferencesDialog(QSettings& settings, QNetworkProxy& proxy) : QDialog()
{
	general_preferences_tab = new GeneralPreferencesTab(settings);
	proxy_preferences_tab = new ProxyPreferencesTab(proxy);
	PreferencesDialog::setUi();
	connect(ok_button, &QPushButton::clicked, [this, &settings, &proxy]() {
		general_preferences_tab->getSettings(settings);
		proxy_preferences_tab->getProxy(proxy);
		close();
	});
	connect(cancel_button, &QPushButton::clicked, [this]() { close(); } );
}

void PreferencesDialog::setUi()
{
	setModal(true);
	setWindowTitle(tr("Preferences"));
	setWindowIcon(QIcon(":/icons/preferences.svg"));

	tabs = new QTabWidget;
	ok_button = new QPushButton("OK");
	cancel_button = new QPushButton(tr("Cancel"));

	ok_button->setFixedWidth(100);
	cancel_button->setFixedWidth(100);

	auto main_layout = new QVBoxLayout;
	auto buttons_layout = new QHBoxLayout;

	setLayout(main_layout);
	tabs->addTab(general_preferences_tab, tr("General"));
	tabs->addTab(proxy_preferences_tab, tr("Proxy"));
	main_layout->addWidget(tabs);
	main_layout->addLayout(buttons_layout);
	buttons_layout->addWidget(ok_button);
	buttons_layout->addWidget(cancel_button);
}

GeneralPreferencesTab::GeneralPreferencesTab(QSettings& settings) : QWidget()
{
	homepage_edit = new QLineEdit(settings.value("General/Homepage").toString());
	lang_edit = new QComboBox;
	lang_edit->addItem(tr("English"));
	lang_edit->addItem(tr("Russian"));

	if (settings.value("General/Language").toString() == "English")
		lang_edit->setCurrentIndex(EnglishLanguage);
	else if (settings.value("General/Language").toString() == "Russian")
		lang_edit->setCurrentIndex(RussianLanguage);

	auto main_layout = new QVBoxLayout;
	auto homepage_layout = new QHBoxLayout;
	auto lang_layout = new QHBoxLayout;
	setLayout(main_layout);
	main_layout->addLayout(lang_layout);
	main_layout->addLayout(homepage_layout);
	lang_layout->addWidget(new QLabel(tr("Language ")));
	lang_layout->addWidget(lang_edit);
	homepage_layout->addWidget(new QLabel(tr("Homepage ")));
	homepage_layout->addWidget(homepage_edit);
}

void GeneralPreferencesTab::getSettings(QSettings& settings)
{
	if (lang_edit->currentIndex() == EnglishLanguage)
		settings.setValue("General/Language", "English");
	else if (lang_edit->currentIndex() == RussianLanguage)
		settings.setValue("General/Language", "Russian");

	settings.setValue("General/Homepage", homepage_edit->text());
}

ProxyPreferencesTab::ProxyPreferencesTab(QNetworkProxy& proxy) : QWidget()
{
	without_proxy_button = new QRadioButton(tr("Don't use proxy"));
	use_proxy_button     = new QRadioButton(tr("Use proxy"));
	host_edit     = new QLineEdit(proxy.hostName());
	port_edit     = new QSpinBox;
	user_edit     = new QLineEdit;
	password_edit = new QLineEdit;
	port_edit->setRange(21, 9999);
	port_edit->setValue(proxy.port());

	auto main_layout = new QVBoxLayout;
	auto set_host_layout = new QHBoxLayout;

	if (proxy.type() == QNetworkProxy::NoProxy) {
		without_proxy_button->setChecked(true);
		host_edit->setDisabled(true);
		port_edit->setDisabled(true);
	} else
		use_proxy_button->setChecked(true);
	
	setLayout(main_layout);
	main_layout->addWidget(without_proxy_button);
	main_layout->addWidget(use_proxy_button);
	main_layout->addLayout(set_host_layout);
	set_host_layout->addWidget(new QLabel(tr("Host ")));
	set_host_layout->addWidget(host_edit);
	set_host_layout->addWidget(new QLabel(tr("port ")));
	set_host_layout->addWidget(port_edit);

	connect(use_proxy_button, &QRadioButton::toggled, [this]() {
		host_edit->setEnabled(true);
		port_edit->setEnabled(true);
	});

	connect(without_proxy_button, &QRadioButton::toggled, [this]() {
		host_edit->setDisabled(true);
		port_edit->setDisabled(true);
	});
}

void ProxyPreferencesTab::getProxy(QNetworkProxy& proxy)
{
	if (without_proxy_button->isChecked())
		proxy.setType(QNetworkProxy::NoProxy);
	else
		proxy.setType(QNetworkProxy::HttpProxy);
	proxy.setHostName(host_edit->text());
	proxy.setPort(port_edit->text().toInt());
}