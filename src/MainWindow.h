#pragma once

#include <QLayout>
#include <QLineEdit>
#include <QMainWindow>
#include <QProgressBar>
#include <QToolBar>
#include <QStatusBar>
#include <QWebEngineHistory>
#include <QWebEngineView>

#include "PreferencesDialog.h"
#include "ProxyAuthDialog.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

	QAction* back_action;
	QAction* forward_action;
	QAction* reload_action;
	QAction* preferences_action;

	QLineEdit* address_edit;
	QWebEngineView* web_widget;
	QToolBar* toolbar;
	QProgressBar* progress_bar;
	QSettings& settings;
	QNetworkProxy proxy;

	PreferencesDialog* preferences_dialog;

public:
	MainWindow(QSettings& settings);
	~MainWindow();

private:
	void readSettings();
	void writeSettings();
	void setUi();
};
