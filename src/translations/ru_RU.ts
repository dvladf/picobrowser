<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>GeneralPreferencesTab</name>
    <message>
        <location filename="../PreferencesDialog.cpp" line="45"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="46"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="59"/>
        <source>Language </source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="61"/>
        <source>Homepage </source>
        <translation>Домашняя страница</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.cpp" line="116"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="117"/>
        <source>Forward</source>
        <translation>Вперёд</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="118"/>
        <source>Reload</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="119"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../PreferencesDialog.cpp" line="19"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="24"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="33"/>
        <source>General</source>
        <translation>Основные</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="34"/>
        <source>Proxy</source>
        <translation>Прокси</translation>
    </message>
</context>
<context>
    <name>ProxyAuthDialog</name>
    <message>
        <location filename="../ProxyAuthDialog.cpp" line="6"/>
        <source>Proxy Authentication</source>
        <translation>Аутентификация прокси</translation>
    </message>
    <message>
        <location filename="../ProxyAuthDialog.cpp" line="11"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../ProxyAuthDialog.cpp" line="27"/>
        <source>Username	</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="../ProxyAuthDialog.cpp" line="29"/>
        <source>Password	</source>
        <translation>Пароль</translation>
    </message>
</context>
<context>
    <name>ProxyPreferencesTab</name>
    <message>
        <location filename="../PreferencesDialog.cpp" line="77"/>
        <source>Don&apos;t use proxy</source>
        <translation>Не использовать прокси</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="78"/>
        <source>Use proxy</source>
        <translation>Использовать прокси</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="100"/>
        <source>Host </source>
        <translation>Хост</translation>
    </message>
    <message>
        <location filename="../PreferencesDialog.cpp" line="102"/>
        <source>port </source>
        <translation>порт</translation>
    </message>
</context>
</TS>
