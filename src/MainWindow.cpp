#include "MainWindow.h"

MainWindow::MainWindow(QSettings& settings) : QMainWindow(), settings(settings)
{
	MainWindow::setUi();
	MainWindow::readSettings();

	preferences_dialog = new PreferencesDialog(settings, proxy);

	connect(back_action, &QAction::triggered, [this]() {
		web_widget->page()->history()->back();
	});

	connect(forward_action, &QAction::triggered, [this]() {
		web_widget->page()->history()->forward();
	});

	connect(reload_action, &QAction::triggered, [this]() {
		web_widget->reload();
	});

	connect(address_edit, &QLineEdit::returnPressed, [this]() {
		web_widget->load(QUrl(address_edit->text()));
	});

	connect(preferences_action, &QAction::triggered, [this]() {
		preferences_dialog->show();
	});

	connect(web_widget->page(), &QWebEnginePage::proxyAuthenticationRequired, [this](const QUrl&, QAuthenticator* auth,
		const QString&) 
	{
		ProxyAuthDialog d(auth);
		d.exec();
	});

	connect(web_widget->page(), &QWebEnginePage::urlChanged, [this](const QUrl& url) {
		address_edit->setText(url.toString());
	});

	connect(web_widget->page(), &QWebEnginePage::titleChanged, [this](const QString& title) {
		setWindowTitle("PicoBrowser - " + title);
	});

	connect(web_widget->page(), &QWebEnginePage::linkHovered, [this](const QUrl& url) {
		statusBar()->showMessage(url.toString());
	});

	connect(web_widget->page(), &QWebEnginePage::loadStarted, [this]() {
		progress_bar->setHidden(false);
	});

	connect(web_widget->page(), &QWebEnginePage::loadFinished, [this]() {
		progress_bar->setHidden(true);
	});

	connect(web_widget->page(), &QWebEnginePage::loadProgress, [this](int progress) {
		progress_bar->setValue(progress);
	});
}

MainWindow::~MainWindow()
{
	MainWindow::writeSettings();
}

void MainWindow::readSettings()
{
	resize(QSize(settings.value("Window/Width").toInt(), 
		settings.value("Window/Height").toInt()));

	if (settings.value("Window/Maximized").toBool()) {
		showMaximized();
	}

	if (!settings.value("Proxy/Used").toBool()) {
		proxy.setType(QNetworkProxy::NoProxy);
	} else {
		proxy.setHostName(settings.value("Proxy/Host").toString());
		proxy.setPort(settings.value("Proxy/Port").toInt());
		proxy.setType(QNetworkProxy::HttpProxy);
	}
	QNetworkProxy::setApplicationProxy(proxy);
	address_edit->setText("http://" + settings.value("General/Homepage").toString());
	web_widget->load(QUrl(address_edit->text()));
}

void MainWindow::writeSettings()
{
	settings.setValue("Window/Width", size().width());
	settings.setValue("Window/Height", size().height());
	settings.setValue("Window/Maximized", isMaximized());
	if (proxy.type() == QNetworkProxy::NoProxy)
		settings.setValue("Proxy/Used", false);
	else
		settings.setValue("Proxy/Used", true);
	settings.setValue("Proxy/Host", proxy.hostName());
	settings.setValue("Proxy/Port", proxy.port());
}

void MainWindow::setUi()
{
	setWindowTitle("PicoBrowser");

	address_edit = new QLineEdit;
	web_widget = new QWebEngineView;
	toolbar = new QToolBar;
	progress_bar = new QProgressBar;
	progress_bar->setHidden(true);

	toolbar->setFloatable(false);
	toolbar->setMovable(false);
	addToolBar(toolbar);
	setCentralWidget(web_widget);

	back_action = new QAction(QIcon(":/icons/go-previous.svg"), tr("Back"));
	forward_action = new QAction(QIcon(":/icons/go-next.svg"), tr("Forward"));
	reload_action = new QAction(QIcon(":/icons/view-refresh.svg"), tr("Reload"));
	preferences_action = new QAction(QIcon(":/icons/preferences.svg"), tr("Preferences"));

	back_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_Left));
	forward_action->setShortcut(QKeySequence(Qt::ALT + Qt::Key_Right));
	reload_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_R));

	toolbar->addAction(back_action);
	toolbar->addAction(forward_action);
	toolbar->addSeparator();
	toolbar->addAction(reload_action);
	toolbar->addWidget(address_edit);
	toolbar->addAction(preferences_action);

	address_edit->setFixedHeight(28);
	address_edit->setStyleSheet("font: 14px");
	
	statusBar()->setFixedHeight(20);
	statusBar()->setStyleSheet("font: 12px");
	statusBar()->addPermanentWidget(progress_bar);
}