#pragma once

#include <QAuthenticator>
#include <QDialog>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QNetworkProxy>
#include <QPushButton>

class ProxyAuthDialog : public QDialog
{
	Q_OBJECT

	QLineEdit* user_edit;
	QLineEdit* password_edit;
	QPushButton* ok_button;
	QPushButton* cancel_button;

public:
	ProxyAuthDialog(QAuthenticator* auth);
};